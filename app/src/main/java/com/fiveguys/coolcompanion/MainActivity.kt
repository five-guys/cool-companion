package com.fiveguys.coolcompanion

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.fiveguys.coolcompanion.ble.BleConnectionState
import com.fiveguys.coolcompanion.ble.BleListener
import com.fiveguys.coolcompanion.ble.BleManager
import kotlinx.android.synthetic.main.activity_main.*


private const val LOGGING_TAG = "#blub MAIN"

private const val ALPHA_FREEZER_ON = 1f
private const val ALPHA_FREEZER_OFF = .2f

private const val BUTTON_DEFAULT_SCALE = 1f
private const val BUTTON_PRESSED_SCALE = .9f

private const val PROPERTY_TRANSLATION_X = "translationX"
private const val TRANSLATION_DURATION = 500L
private const val PROPERTY_ROTATION = "rotation"
private const val ROTATION_DURATION = 5000L

private const val FAN_PIN = 15
private const val PELTIER_PIN = 16
private const val FREEZER_PIN = 17

private const val LOW = 0
private const val HIGH = 1

private const val ENABLE_BLUETOOTH_REQUEST_CODE = 45324

class MainActivity : AppCompatActivity() {

    private lateinit var bleManager: BleManager
    private val bleListener = object : BleListener {
        override fun onMessageReceived(message: String) {
            val split = message.split(' ')
            if (split.size != 2) {
                Log.d(
                    LOGGING_TAG, "Wrong message format, expected: \"PREFIX MESSAGE\" " +
                            "(where message doesn't contain spaces)"
                )
                return
            }
            when (split[0].toUpperCase()) {
                "T" -> {
                    runOnUiThread {
                        txtTempVal.text =
                            String.format(getString(R.string.temperature_celsius), split[1].toFloat())
                    }
                }
                "F" -> {
                    runOnUiThread {
                        imgFreezerState.alpha =
                            if (split[1].toInt() == HIGH) ALPHA_FREEZER_ON
                            else ALPHA_FREEZER_OFF
                    }
                }
                else -> return
            } 
        }

        override fun onConnectionStateChanged(state: BleConnectionState) {
            when (state) {
                BleConnectionState.Connected -> {
                    runOnUiThread {
                        resetTranslation(viewContainer)
                        moveOffScreen(txtConnect, -1)
                        logoAnimator?.repeatCount = 0
                        logoAnimator = null
                    }
                }
                BleConnectionState.Disconnected -> {
                    runOnUiThread {
                        txtTempVal.text = getString(R.string.no_data)
                        imgFreezerState.alpha = ALPHA_FREEZER_OFF

                        moveOffScreen(viewContainer)
                        resetTranslation(txtConnect)

                        rotateLogo()
                    }
                }
            }
        }
    }

    private var logoAnimator: ObjectAnimator? = null

    //region Android Lifcycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        verifyPermissions()
        verifyBluetoothEnabled()

        initButtons()

        txtConnect.visibility = View.INVISIBLE
        viewContainer.visibility = View.INVISIBLE
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        // enable fullscreen on activity created
        supportActionBar?.hide()
        clRoot.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    //endregion

//region Permissions & Bluetooth Initialization

    private fun verifyPermissions() {
        val permissions = mutableListOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.BLUETOOTH
        )

        permissions.removeAll {
            ActivityCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }

        if (permissions.count() > 0)
            ActivityCompat.requestPermissions(this, permissions.toTypedArray(), 1)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(applicationContext)) {
                val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
                intent.data = Uri.parse("package:$packageName")
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                this.startActivity(intent)
            }
        }
    }

    private fun verifyBluetoothEnabled() {
        if (!BleManager.isBluetoothEnabled()) {
            startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), ENABLE_BLUETOOTH_REQUEST_CODE)
        } else {
            bleManager = BleManager(applicationContext, bleListener)
            onBleManagerInitialized()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ENABLE_BLUETOOTH_REQUEST_CODE) {
            verifyBluetoothEnabled()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onBleManagerInitialized() {
        if (clRoot.width != 0) { // TODO find other way to check
            initView()
        } else {
            clRoot.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    initView()
                    clRoot.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }

    }
//endregion

    //region Layout
    private fun initView() {
        txtConnect.visibility = View.VISIBLE
        viewContainer.visibility = View.VISIBLE

        txtTempVal.text = getString(R.string.no_data)
        imgFreezerState.alpha = ALPHA_FREEZER_OFF

        when (bleManager.connectionState) {
            BleConnectionState.Connected -> {
                resetTranslation(viewContainer, true)
                moveOffScreen(txtConnect, -1, true)
                logoAnimator?.repeatCount = 0
                logoAnimator = null
            }
            BleConnectionState.Disconnected -> {
                moveOffScreen(viewContainer, 1, true)
                resetTranslation(txtConnect, true)
                rotateLogo()
            }
        }
    }

    private fun initButtons() {

        btnToggleFreezer.setOnClickListener {
            bleManager.sendMessage(FREEZER_PIN.toString())
        }
        btnToggleFreezer.setOnTouchListener { view, event ->
            onButtonTouch(view, event)
            false
        }

        btnDebugFan.setOnTouchListener { view, event ->
            onDebugButtonTouch(view, event, FAN_PIN.toString())
            false
        }

        btnDebugPeltier.setOnTouchListener { view, event ->
            onDebugButtonTouch(view, event, PELTIER_PIN.toString())
            false
        }
    }

    private fun onButtonTouch(view: View, event: MotionEvent) {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                view.scaleX = BUTTON_PRESSED_SCALE
                view.scaleY = BUTTON_PRESSED_SCALE
            }
            MotionEvent.ACTION_UP -> {
                view.scaleX = BUTTON_DEFAULT_SCALE
                view.scaleY = BUTTON_DEFAULT_SCALE
            }
        }
    }

    private fun onDebugButtonTouch(view: View, event: MotionEvent, message: String) {
        onButtonTouch(view, event)

        when (event.action) {
            MotionEvent.ACTION_DOWN -> bleManager.sendMessage(message)
            MotionEvent.ACTION_UP -> bleManager.sendMessage(message)
        }
    }
    //endregion

    //region Animation
    private fun moveOffScreen(view: View, direction: Int = 1, skipAnimation: Boolean = false) {
        setViewTranslationX(view, direction * clRoot.width.toFloat(), skipAnimation)
    }

    private fun resetTranslation(view: View, skipAnimation: Boolean = false) {
        setViewTranslationX(view, 0f, skipAnimation)
    }

    private fun setViewTranslationX(view: View, translationX: Float, skipAnimation: Boolean = false) {
        ObjectAnimator.ofFloat(view, PROPERTY_TRANSLATION_X, translationX).apply {
            duration = if (skipAnimation) 0 else TRANSLATION_DURATION
            start()
        }
    }

    private fun rotateLogo() {
        logoAnimator?.cancel()
        imgLogo.rotation = 0f

        runOnUiThread {
            logoAnimator = rotate(imgLogo, 360f)
        }
    }

    private fun rotate(view: View, angle: Float): ObjectAnimator? {
        return ObjectAnimator.ofFloat(view, PROPERTY_ROTATION, angle).apply {
            duration = ROTATION_DURATION
            repeatMode = ValueAnimator.RESTART
            repeatCount = ValueAnimator.INFINITE
            start()
        }
    }
//endregion
}
