package com.fiveguys.coolcompanion.ble

import java.util.*

interface BleListener {
    fun onMessageReceived(message: String){}
    fun onConnectionStateChanged(state: BleConnectionState){}
}