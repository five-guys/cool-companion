package com.fiveguys.coolcompanion.ble

import android.Manifest
import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import java.util.*
import android.bluetooth.BluetoothGattDescriptor




private const val LOGGING_TAG = "#blub BLE"
private const val MAX_MESSAGE_BYTES = 20

private const val ADAFRUIT_NAME = "Adafruit Bluefruit LE"

enum class BleConnectionState { Connected, Disconnected }

class BleManager(private val context: Context, private val listener: BleListener) {
    private val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

    private val bluetoothAdapter = bluetoothManager.adapter
    private lateinit var gattServer: BluetoothGatt

    var connectionState = BleConnectionState.Disconnected
        private set(value) {
            if (field == value)
                return

            field = value
            listener.onConnectionStateChanged(value)
        }

    private var currentDevice: BluetoothDevice? = null
    private lateinit var bluetoothScanner: BluetoothLeScanner
    private lateinit var scanCallback: BleScanCallback
    private var isScanning: Boolean = false
    private val scanResults: MutableMap<String, BluetoothDevice> = mutableMapOf()

    private inner class GattClientCallback : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)

            if (status != BluetoothGatt.GATT_SUCCESS || newState == BluetoothProfile.STATE_DISCONNECTED) {
                disconnectBluetoothGatt(gatt)
                startScan()
            } else if (newState == BluetoothProfile.STATE_CONNECTED) {
                connectionState = BleConnectionState.Connected
                gatt?.discoverServices()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Log.d(LOGGING_TAG, "Discovering services failed")
                return
            }
            val service: BluetoothGattService? = gatt?.getService(GATTSERVICE_UUID)

            val rx = service?.getCharacteristic(RX_UUID)

            gatt?.setCharacteristicNotification(rx, true)

            val desc = rx?.getDescriptor(CLIENT_UUID)
            if (desc == null) {
                Log.d(LOGGING_TAG, "Characteristic has no client descriptor")
                return
            }
            desc.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            if (!gatt.writeDescriptor(desc)) {
                Log.d(LOGGING_TAG, "Failed to write descriptor")
                return
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)

            val messageBytes: ByteArray = characteristic?.value ?: return
            val messageString = String(messageBytes, charset("UTF-8"))

            Log.d(LOGGING_TAG, "Received message: $messageString")

            listener.onMessageReceived(messageString)
        }

    }

    private inner class BleScanCallback : ScanCallback() {

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            onScanResult(result)
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            for (result in results) {
                onScanResult(result)
            }
        }

        override fun onScanFailed(errorCode: Int) {
            Log.e(LOGGING_TAG, "Ble scan failed. Error code: $errorCode")
        }

    }

    private fun onScanResult(result: ScanResult) {
        if (isScanning && result.device != null) {
            if (result.device.name == ADAFRUIT_NAME) {
                connectDevice(result.device)
                stopScan()
            }
        }
    }

    private fun connectDevice(device: BluetoothDevice) {
        val gattClientCallback = GattClientCallback()
        gattServer = device.connectGatt(context, false, gattClientCallback)
        currentDevice = device
    }

    private fun startScan() {
        if (isScanning) {
            Log.w(LOGGING_TAG, "Tried to start device scan but is already scanning")
            return
        }
        if (!hasPermission()) {
            Log.w(LOGGING_TAG, "Tried to start device scan but permissions are missing")
            return
        }
        if (bluetoothAdapter == null || bluetoothAdapter.bluetoothLeScanner == null) {
            Log.w(LOGGING_TAG, "Tried to start device scan but BleManager has not been properly initialized")
            return
        }

        if (::gattServer.isInitialized) {
            gattServer.disconnect()
            gattServer.close()
        }

        scanResults.clear()
        currentDevice = null

        val filters: MutableList<ScanFilter> = ArrayList()
        val scanFilter: ScanFilter = ScanFilter.Builder()
            .build()
        filters.add(scanFilter)
        val settings: ScanSettings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build()

        scanCallback = BleScanCallback()
        bluetoothScanner = bluetoothAdapter.bluetoothLeScanner
        bluetoothScanner.startScan(filters, settings, scanCallback)
        isScanning = true
    }


    private fun hasPermission(): Boolean {
        return ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.BLUETOOTH
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun stopScan() {
        if (isScanning && bluetoothAdapter.isEnabled && ::bluetoothScanner.isInitialized) {
            bluetoothScanner.stopScan(scanCallback)
            isScanning = false
        }
    }

    private fun disconnectBluetoothGatt(gatt: BluetoothGatt?) {
        gatt?.let {
            if (it.device == currentDevice) {
                connectionState = BleConnectionState.Disconnected
            }

            gatt.disconnect()
            gatt.close()
        }
    }

    fun sendMessage(message: String) {
        if (connectionState == BleConnectionState.Disconnected) {
            Log.w(LOGGING_TAG, "Tried to send message, but no device is connected")
            return
        }

        if(!::gattServer.isInitialized){
            Log.w(LOGGING_TAG, "Tried to send message, but no gattServer is not initialized")
            return
        }

        val service = gattServer.getService(GATTSERVICE_UUID)
        if (!service.characteristics.any { (it.uuid == TX_UUID) }) {
            Log.w(LOGGING_TAG, "Tried to send message, but service doesn't contain the specified uuid ($TX_UUID)")
            return
        }

        val messageBytes = message.toByteArray()
        if (messageBytes.size > MAX_MESSAGE_BYTES) {
            Log.w(
                LOGGING_TAG,
                "Tried to send message, but message size (${messageBytes.size}B) exceeds max size (${MAX_MESSAGE_BYTES}B)"
            )
            return
        }

        Log.d(LOGGING_TAG, "Sending message: $TX_UUID => $message")

        val characteristic: BluetoothGattCharacteristic = service.getCharacteristic(TX_UUID)
        characteristic.value = messageBytes
        gattServer.writeCharacteristic(characteristic)
    }

    init {
        startScan()
    }

    companion object {
        fun isBluetoothEnabled(): Boolean {
            return BluetoothAdapter.getDefaultAdapter().isEnabled
        }
    }
}
